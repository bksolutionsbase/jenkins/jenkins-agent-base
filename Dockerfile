FROM registry.access.redhat.com/ubi9/buildah:latest

ARG user=build
ARG group=build

ARG PORT=10022
ARG PATHWORK=/home/"${user}"


##  Add sshd configuration file(s) to container
ADD ./sshd.conf/*.conf /etc/ssh/sshd_config.d/

##  Extend, install and configure container system
RUN dnf update -y \
&&  dnf install -y openssh-server java-17-openjdk-headless skopeo \
&&  dnf clean all \
&&  sed -e "s/Port.*/Port ${PORT}/g" -i /etc/ssh/sshd_config.d/81-host.conf \
&&  semanage port -a -t ssh_port_t -p tcp ${PORT} \
&&  /usr/libexec/openssh/sshd-keygen ed25519 \
&&  install -d "${PATHWORK}"/.ssh --mode 700 --owner "${user}" --group "${group}" \
&&  install -d "${PATHWORK}"/jenkins --mode 700 --owner "${user}" --group "${group}"

ARG PATHJENKINS=/usr/share/jenkins
ARG VERSION=3085.vc4c6977c075a
ADD --chown="${user}":"${group}" "https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/${VERSION}/remoting-${VERSION}.jar" "${PATHJENKINS}"/agent.jar
RUN chmod 0644 "${PATHJENKINS}"/agent.jar \
&&  ln -sf "${PATHJENKINS}"/agent.jar "${PATHJENKINS}"/slave.jar 



EXPOSE ${PORT}

VOLUME ["${PATHWORK}"]
WORKDIR "${PATHWORK}"/jenkins

ENTRYPOINT ["/usr/sbin/sshd"]
CMD ["-D", "-e"]
